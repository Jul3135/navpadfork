%% Example: Greedily select paths without safety considerations
clc
clear 
close all

%% Add required paths
addpath(genpath('../../fastMarching'));
addpath(genpath('../../maps'));
addpath(genpath('../../normalPathLibrary'));
addpath(genpath('../../gridmapping'));
addpath(genpath('../../rayCasting'));
addpath(genpath('../../featureExtractor'));
addpath(genpath('utils'));

%% Get parameters
grid_params = get_grid_params(1);
laser_params = get_laser_params(2);
planner_params = get_planner_params(1);
learner_params = get_learner_params(1, grid_params);

%% Initialize problem statement
problem.start.x = 34; problem.start.y = 20; problem.start.psi = pi/2;
problem.goal.x = 209; problem.goal.y = 53; problem.goal_tol = 14; 

%% Get world map and robot's belief map
[ world_map, robo_map ] = get_map_obj( '../../maps/nickroymapsmall.png', grid_params, problem.start, -[problem.start.x problem.start.y; problem.start.x problem.start.y] + [1 1; 85 60]);

%% Get display object
display_obj = get_display_obj( problem, world_map, planner_params, false );

pause
%% Main loop

training_features = [];
training_labels = [];
while (1)
    found_path = false;
    state = problem.start;
    history = state;
    feature_hist = [];
    feature_last = [];
    feature_last_traj = [];
    backup_hist = [];
    learner_params.weight
    while (~has_reached_goal(state, problem))
        % Update belief map
        robo_map = update_belief( robo_map, state, world_map, laser_params, grid_params, display_obj );
        
        % Plan
        [chosen_path, valid_plan] = select_plan( state, problem, robo_map, planner_params, display_obj, learner_params);
        
        if (~valid_plan)
            display('no plan found');
            training_features = [training_features feature_last_traj];
            training_labels = [training_labels ones(1, size(feature_last_traj,2))];
            learner_params = learner_params.update_learner( training_features, training_labels, learner_params );
            learner_params.weight
            break;
        end
        
        % Call dynamics
        [state, history] = forward_sim_trajectory( state, history, chosen_path, planner_params, display_obj );
        crashed = false;
        for i = (length(backup_hist)+1):length(history)
            if (collision_check_binary( world_map.data, history(i).x, history(i).y ))
                display('crashed');
                crashed = true;
                break;
            end
        end
        if (crashed)
            training_features = [training_features feature_last_traj];
            training_labels = [training_labels ones(1, size(feature_last_traj,2))];
            learner_params = learner_params.update_learner( training_features, training_labels, learner_params );
            learner_params.weight
            break;
        end

        pause(0.1);
        if (has_reached_goal(state_at_index( chosen_path, length(chosen_path.x) ), problem))
            found_path = true;
            break;
        end
        
        feature_last = learner_params.feature(state, robo_map);
        feature_last_traj = learner_params.feature(chosen_path, robo_map);
        feature_hist = [feature_hist feature_last];
        backup_hist = history;
    end
    
    if (found_path)
        display('success');
        training_features = [training_features feature_hist];
        training_labels = [training_labels -1*ones(1, size(feature_hist,2))];
        learner_params = learner_params.update_learner( training_features, training_labels, learner_params );
    end
    
    problem.start.x = 34 + (randi(50) - 25); problem.start.y = 20 + (randi(40) - 20);
    [ world_map, robo_map ] = get_map_obj( '../../maps/nickroymapsmall.png', grid_params, problem.start, -[problem.start.x problem.start.y; problem.start.x problem.start.y] + [1 1; 85 60]);
end

clean_display( display_obj );