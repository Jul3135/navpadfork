%% Example: Greedily select paths without safety considerations
clc
clear 
close all

%% Add required paths
addpath(genpath('../../fastMarching'));
addpath(genpath('../../maps'));
addpath(genpath('../../normalPathLibrary'));
addpath(genpath('../../gridmapping'));
addpath(genpath('../../rayCasting'));
addpath(genpath('../../abortPathLibrary'));
%% Load an emergancy maneuver library
load ../../abortPathLibrary/abortPathLibrarySmall.mat;
queryState = [12,0,0]; % this is just for test purposes and is temporary
check_safety = true;
%% Load a map
map = getMapfromImage('../../maps/example_map.tif');
%map = getMapfromImage('../../maps/pacmanMap.jpg');
%map = getMapfromImage('../../maps/map_3sb.jpg');
map(map < 1.0) = 0;

%% Grid parameters
grid_params = struct();
grid_params.p_occ_default = 0.5;                                %default occupancy value
grid_params.p_empty_default = 1 - grid_params.p_occ_default;  
grid_params.p_occ_sensor = 0.95;                                %occupancy increase on hit
grid_params.p_empty_sensor = 0.60;                              %occupancy decrease on miss


%% Laser Parameters
laser_params = struct();
laser_params.max_range = 200;                              %laser range
laser_params.range_resolution = 0.8;                        %range resolution
laser_params.laser_resolution = 1/laser_params.max_range;   %angle resolution
laser_params.laser_fov = [-0.5,0.5];                            %field of view

%% Initialize grid
% Pre-Calculating grid update numbers
grid_params.log_odds_occ = log(grid_params.p_occ_default/grid_params.p_empty_default);
grid_params.log_odds_occ_sensor = log(grid_params.p_occ_sensor/(1-grid_params.p_occ_sensor));
grid_params.log_odds_empty_sensor = log((1-grid_params.p_empty_sensor)/grid_params.p_empty_sensor);

% Initializing maps and grids
world_map.data = map;
world_map.scale = 1;
world_map.min = [1,1];
robo_map = grid_params.log_odds_occ*ones(size(map));
bounding_box = [-70,-70;70,70];

%% Initialize planning
load ../normalPathLibrary/gk_path_set.mat;
planner_params = struct();
planner_params.path_set = gk_path_set;                                  %Which path set
planner_params.ind = round(length(planner_params.path_set(1).x)*0.1);   %Replan index

f_cost = @(path) sum(sqrt(diff(path.x).^2+diff(path.y).^2));            %Cost handle
f_heur = @get_value;                                                    %Heuristic handle
f_coll = @collision_check_binary;                                       %Collision check handle

%% Initialize problem statement
state.x = 350; 
state.y = 500;
state.psi = -pi/2;

%state.x = 40;
%state.y = 20;
%state.psi = 0;

%state.x = 20; state.y = 20; state.psi = pi/2;

goal.x = 238;
goal.y = 344;

%goal.x = 482;
%goal.y = 475;

% note seems like x and y are reversed?
% goal.x = 35; goal.y = 366;

goal_tol = 10; %How close to goal is considered reaching it?

history = state;
robo_map = set_boundingbox_to_value( -100, robo_map, [state.x state.y], bounding_box, world_map);
%% Configure display
handle_set = struct();
% Figure 1: Main display
handle_set.fig_main = figure;
hold on;
colormap(gray);
clims = [0 1];
handle_set.occ_map = imagesc(zeros(size(map')), clims);
handle_set.path_set = plot_path_set( transform_path_set(  state, planner_params.path_set ) );
handle_set.chosen = plot_path( state );
handle_set.history = plot_history( history);
handle_set.start_goal = plot_start_goal(state,goal,goal_tol,world_map);
handle_set.safety_library = displayMapVelocityPaths([state.x state.y state.psi],[],queryState,stateMatrix,globalPathList,...
                        globalPathIds,minState,resolutionState,world_map.min(1),world_map.min(2),world_map.scale);
handle_set.safe_paths = [];
axis xy;
axis equal;

% Figure 2: Heuristic display
handle_set.fig_heur = figure;
handle_set.heur_map = imagesc(zeros(size(map')));
axis xy;
axis equal;

% Figure 3: WorldMap display
handle_set.fig_world = figure;
handle_set.world_map = imshow(map');
handle_set.world_path_set = plot_path_set( transform_path_set(  state, planner_params.path_set ) );
handle_set.world_chosen = plot_path( state );
handle_set.world_history = plot_history( history);
handle_set.world_start_goal = plot_start_goal(state,goal,goal_tol,world_map);
axis xy;
axis equal;


% If video to be recorded
record_video = false;
if (record_video)
    vidObj1 = VideoWriter('vid_robot.avi');
    open(vidObj1);
    vidObj2 = VideoWriter('vid_heur.avi');
    open(vidObj2);
end

while (norm([state.x state.y] - [goal.x goal.y]) > goal_tol)
    % Update belief map
    [trace_x, trace_y, trace_r, trace_collision] = fast_trace(world_map, laser_params.laser_fov, laser_params.laser_resolution, laser_params.max_range, laser_params.range_resolution, ...
                                                              [state.x state.y], state.psi);
    robo_map = update_robo_world(laser_params.laser_fov, laser_params.laser_resolution, laser_params.range_resolution, ...
                                 trace_x, trace_y, trace_r, trace_collision, ...
                                 [state.x state.y], state.psi, robo_map, world_map,grid_params);
    [ p, p_ ] = logodds2prob( robo_map);
    set(handle_set.occ_map,'CData', p_');
    
    % Update heuristic
    threshold_map = p_;
    threshold_map(threshold_map < 0.1) = 0;
    threshold_map(threshold_map > 0.1) = 1;
    heur = get_value_function( threshold_map, goal.x, goal.y );
    set(handle_set.heur_map,'CData', heur');
    
    % Tranform path to robot state
    tf_path_set = transform_path_set(  state, planner_params.path_set );
    handle_set.path_set = plot_path_set( tf_path_set, handle_set.path_set );
    handle_set.world_path_set = plot_path_set( tf_path_set, handle_set.world_path_set );
    
    % Displaying emergency maneuver library
    figure(handle_set.fig_main);
    delete(handle_set.safety_library);
    handle_set.safety_library = displayMapVelocityPaths([state.x state.y state.psi],[],queryState,stateMatrix,globalPathList,...
                        globalPathIds,minState,resolutionState,world_map.min(1),world_map.min(2),world_map.scale);
    validPaths = queryVelocityFeasability2([state.x state.y state.psi],p_,stateMatrix,queryState,minState,...
                        resolutionState,world_map.min(1),world_map.min(2),world_map.scale,globalPathList,globalPathIds);
    
    delete(handle_set.safe_paths);
    handle_set.safe_paths = displayPathsByIdsonMap([state.x state.y state.psi],globalPathList,globalPathIds,validPaths,...
                        world_map.scale,world_map.min(1),world_map.min(2),'g');


    
    % Sort path by: f_cost + f_heuristic
    criteria = [];
    for i=1:length(tf_path_set)
        criteria = [criteria; i f_cost(tf_path_set(i)) + f_heur( heur, tf_path_set(i).x(end), tf_path_set(i).y(end))];
    end
    criteria = sortrows(criteria,2);
    tf_path_set = tf_path_set(round(criteria(:,1)));

    % Select collision free trajectories
    collision_free_path_ids = [];
    for i = 1:length(tf_path_set)
        collision = false;
        for j = 1:length(tf_path_set(i).x)
            if (f_coll(threshold_map, tf_path_set(i).x(j), tf_path_set(i).y(j)))
                collision = true;
                break;
            end
        end
        if (~collision)
            collision_free_path_ids = [collision_free_path_ids;i];
        end
    end    
    
    % Select best safe trajectory
    tf_path_set = tf_path_set(collision_free_path_ids);
    chosen_path = [];
    if(check_safety)
        for i = 1:length(tf_path_set)
            test_state = tf_path_set(i);
            for field = fieldnames(state)'
                test_state.(field{1}) = tf_path_set(i).(field{1})(planner_params.ind);
            end

            validPaths = queryVelocityFeasability2([test_state.x test_state.y test_state.psi],p_,stateMatrix,queryState,minState,...
                            resolutionState,world_map.min(1),world_map.min(2),world_map.scale,globalPathList,globalPathIds);
            safe = ~isempty(validPaths);        
             if (safe)
                chosen_path = tf_path_set(i);
                break;
            end
        end
    else
        if ~isempty(tf_path_set)
            chosen_path = tf_path_set(1);
        end
    end
    

    % If no path then failed
    if(isempty(chosen_path))
        disp('Failure: None of the future states are safe will execute an emergency maneuver');
        break;
    end
    
    % Update state and history
    state = struct(chosen_path);
    history_increment = struct(chosen_path);
    for field = fieldnames(state)'
        state.(field{1}) = chosen_path.(field{1})(planner_params.ind);
        history_increment.(field{1}) = chosen_path.(field{1})(1:planner_params.ind);
    end
    history = [history history_increment];
    handle_set.chosen = plot_path( chosen_path, handle_set.chosen);
    handle_set.history = plot_history( history, handle_set.history);
    handle_set.world_chosen = plot_path( chosen_path, handle_set.world_chosen);
    handle_set.world_history = plot_history( history, handle_set.world_history);
    
    %Video recording
    if (record_video)
        writeVideo(vidObj1,getframe(handle_set.fig_main));
        writeVideo(vidObj2,getframe(handle_set.fig_heur));
    end
    
    pause(0.1);
end

%Close video files
if (record_video)
    close(vidObj1);
    close(vidObj2);
end