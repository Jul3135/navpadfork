

width = 200;
height = 100;

wmap = zeros(height, width);

num_corners = 10;

xs = [1, sort(randperm(width-2, num_corners)+1), width];
ys = [1, sort(randperm(height-2, num_corners)+1), height];

num_rect = 8;
for i=1:num_rect
    x_ix = randi( numel(xs)-1 );
    y_ix = randi( numel(ys)-1 );

    x0 = xs(x_ix); x1 = xs(x_ix+1);
    y0 = ys(y_ix); y1 = ys(y_ix+1);

    wmap(y0:y1, x0:x1) = 1
end

%se = strel('disk', 1);
wmap = imerode(wmap, se);

% add border
wmap = padarray(wmap, [1 1], 1);

imshow(wmap);

imshow(randomRectangles(200, 100, 10, 10, 20))

