function [ transformedPoints ] = transformPoint2Global(p,rotationAngle,translation)
%TRANSFORMPOINT2GLOBAL Summary of this function goes here
%   Detailed explanation goes here
R = [cos(rotationAngle) -sin(rotationAngle);...
     sin(rotationAngle) cos(rotationAngle)];
transformedPoints = bsxfun(@plus,(R*p')',translation);
end

